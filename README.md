# Nitrux Base Files [![Build Status](https://travis-ci.org/Nitrux/nitrux-base-files.svg?branch=master)](https://travis-ci.org/Nitrux/nitrux-base-files)

Nitrux LSB system miscellaneous file.

![](https://i.imgur.com/H6UEtq9.png)

# Issues
If you find problems with the contents of this repository please create an issue.

©2018 Nitrux Latinoamericana S.C.
